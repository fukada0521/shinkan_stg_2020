/**
深田のメモ用です

クラスの構造

Gameobject
 - Character 
 　　　　- Enemy
         - EnemyShotCircle
         - Player      
 - Bullet  
 　　　　- Laser
 


Gameobject(x座標とy座標と当たり半径)
 　　- Character(HPとダメージ計算)
 　　　　　　- Enemy(角度、スケール、画像、移動と攻撃)
             - EnemyShotCircle(円形攻撃をする敵の移動と攻撃)
             - Player()
             
　　 - Bullet(弾を動かす、表示する、消去する)
             - Laser(レーザーの大きさ、表示)



void setup() {
  画面サイズ
  フレームレート
  状態決定
  配列の初期化
  オブジェクトの初期位置
  画像のロード
}


void draw() {
  状態1：はじめにタイトル
  状態2：本編
  状態3：エンディング
}




ーーーーーーーーーーーーーーーーーー状態１ーーーーーーーーーーーーーーーーーーーーーーーーーー
int title(){
  setup();
  textSize(30);
  fill(0);
  text("Press SPACE key to start", width * 0.15, height * 0.5);
  if(keyPressed && key == ' '){
    return 1;
  }
  return 0;
}



キーの押し離し
void keyPressed() {
  if (key == 'w') {
    keyUp = true;
    return;
  }

  if (key == 'a') {
    keyLeft = true;
    return;
  }

  if (key == 'd') {
    keyRight = true;
    return;
  }

  if (key == 's') {
    keyDown = true;
    return;
  }

  if (key == ' ') {
    space = true;
    return;
  }
}

void keyReleased() {
if (key == 'w') {
keyUp = false;
return;
}

if (key == 'a') {
keyLeft = false;
return;
}

if (key == 'd') {
keyRight = false;
return;
}

if (key == 's') {
keyDown = false;
return;
}

if (key == ' ') {
space = false;
return;
}
}



当たり判定をするメソッドです
boolean collision(GameObject gameObject1, GameObject gameObject2) { // 一般的な衝突に利用できる関数となっている
  float distanceX = gameObject1.getX() - gameObject2.getX();
  float distanceY = gameObject1.getY() - gameObject2.getY();
  2乗同士で判定
  float sqDistance = sq(distanceX) + sq(distanceY);
  return sqDistance < sq(gameObject1.getHitRadius() + gameObject2.getHitRadius());
}





ゲームを描画するメソッドです　
ーーーーーーーーーーーーーーーーーー状態2ーーーーーーーーーーーーーーーーーーーーーーー
int game() {
  // ここで画面をいったんリセットしている（全体を白く塗りつぶし）
  fill(255);
  rect(0, 0, width, height);

  //敵弾の移動と描画
  fill(255, 0, 0);
  for (int i = 0; i < bulletList.size(); i++) { //弾の移動と描画を行う
    Bullet bullet = bulletList.get(i);
    bullet.move();
    bullet.draw();
    if (collision(player, bullet)) { // 衝突判定
      bullet.setIsHit(true);
      player.sufferDamage();
    }
    if (bullet.needRemove()){
      bulletList.remove(i);
    }
  }
  
  //自機弾の移動と描画
  fill(0, 0, 0); //自機の打つlaserの色
  for (int i = 0; i < laserList.size(); i++) {
    Laser laser = laserList.get(i);
    laser.move();
    laser.draw();
    if (collision(enemy, laser)) {
      laser.setIsHit(true);
      enemy.sufferDamage();
    }
    if (laser.needRemove()){
      laserList.remove(i);
    }
  }
  
  //敵自身の移動と描画
  fill(167, 87, 168);
  enemy.move();
  enemy.draw();

  //自機自身の移動と描画
  player.update();
  player.draw();
  
  
  // ゲームに決着がついたかどうか判定
  fill(0);
  textSize(10);
  text("Player HP:" + nf(player.getHitPoint(), 3) , 20, 20);
  text("reiwa HP:" + nf(enemy.getHitPoint(), 3)  , 20, 40);
  自機か敵機のHPが0になればゲームを止めます
 if (player.getHitPoint() <= 0 || enemy.getHitPoint() <= 0){
   return 2;
 }
 return 1;
}





ゲームが終わった際に呼び出されるメソッドです　
ーーーーーーーーーーーーーー状態3ーーーーーーーーーーーーーーーーーーーーーーー
int ending(){
  いろんな処理
  return 2;
}










 */
